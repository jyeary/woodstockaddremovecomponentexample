/*
 * SessionBean1.java
 * 
 * Blue Lotus Software, LLC
 * 
 * $Id$
 * 
 * Copyright 2008 John Yeary 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 * 
 * Created on Sep 22, 2008, 2:05:00 PM
 */
package addremovepoc;

import com.sun.rave.web.ui.appbase.AbstractSessionBean;
import com.sun.sql.rowset.CachedRowSetXImpl;
import javax.faces.FacesException;

/**
 * <p>Session scope data bean for your application.  Create properties
 *  here to represent cached data that should be made available across
 *  multiple HTTP requests for an individual user.</p>
 *
 * <p>An instance of this class will be created for you automatically,
 * the first time your application evaluates a value binding expression
 * or method binding expression that references a managed bean using
 * this class.</p>
 *
 * @author John Yeary
 * @version 1.0
 */
public class SessionBean1 extends AbstractSessionBean {

    private static final long serialVersionUID = -445065890316668287L;
    // <editor-fold defaultstate="collapsed" desc="Managed Component Definition">

    /**
     * <p>Automatically managed component initialization.  <strong>WARNING:</strong>
     * This method is automatically generated, so any user-specified code inserted
     * here is subject to being replaced.</p>
     */
    private void _init() throws Exception {
        customerRowSet.setDataSourceName("java:comp/env/jdbc/sample");
        customerRowSet.setCommand("SELECT * FROM APP.CUSTOMER");
        customerRowSet.setTableName("CUSTOMER");
        customerRowSet1.setDataSourceName("java:comp/env/jdbc/sample");
        customerRowSet1.setCommand("SELECT DISTINCT APP.CUSTOMER.STATE  FROM APP.CUSTOMER ORDER BY APP.CUSTOMER.STATE ASC");
        customerRowSet1.setTableName("CUSTOMER");
        customerRowSet2.setDataSourceName("java:comp/env/jdbc/sample");
        customerRowSet2.setCommand("SELECT ALL APP.CUSTOMER.CUSTOMER_ID,  APP.CUSTOMER.NAME FROM APP.CUSTOMER WHERE APP.CUSTOMER.STATE = ? ORDER BY APP.CUSTOMER.NAME ASC");
        customerRowSet2.setTableName("CUSTOMER");
    }
    private CachedRowSetXImpl customerRowSet = new CachedRowSetXImpl();

    public CachedRowSetXImpl getCustomerRowSet() {
        return customerRowSet;
    }

    public void setCustomerRowSet(final CachedRowSetXImpl crsxi) {
        this.customerRowSet = crsxi;
    }
    private CachedRowSetXImpl customerRowSet1 = new CachedRowSetXImpl();

    public CachedRowSetXImpl getCustomerRowSet1() {
        return customerRowSet1;
    }

    public void setCustomerRowSet1(final CachedRowSetXImpl crsxi) {
        this.customerRowSet1 = crsxi;
    }
    private CachedRowSetXImpl customerRowSet2 = new CachedRowSetXImpl();

    public CachedRowSetXImpl getCustomerRowSet2() {
        return customerRowSet2;
    }

    public void setCustomerRowSet2(final CachedRowSetXImpl crsxi) {
        this.customerRowSet2 = crsxi;
    }
    // </editor-fold>

    /**
     * <p>Construct a new session data bean instance.</p>
     */
    public SessionBean1() {
    }

    /**
     * <p>This method is called when this bean is initially added to
     * session scope.  Typically, this occurs as a result of evaluating
     * a value binding or method binding expression, which utilizes the
     * managed bean facility to instantiate this bean and store it into
     * session scope.</p>
     * 
     * <p>You may customize this method to initialize and cache data values
     * or resources that are required for the lifetime of a particular
     * user session.</p>
     */
    @Override
    public void init() {
        // Perform initializations inherited from our superclass
        super.init();
        // Perform application initialization that must complete
        // *before* managed components are initialized
        // TODO - add your own initialiation code here

        // <editor-fold defaultstate="collapsed" desc="Managed Component Initialization">
        // Initialize automatically managed components
        // *Note* - this logic should NOT be modified
        try {
            _init();
        } catch (final Exception e) {
            log("SessionBean1 Initialization Failure", e);
            throw e instanceof FacesException ? (FacesException) e : new FacesException(e);
        }

    // </editor-fold>
    // Perform application initialization that must complete
    // *after* managed components are initialized
    // TODO - add your own initialization code here
    }

    /**
     * <p>This method is called when the session containing it is about to be
     * passivated.  Typically, this occurs in a distributed servlet container
     * when the session is about to be transferred to a different
     * container instance, after which the <code>activate()</code> method
     * will be called to indicate that the transfer is complete.</p>
     * 
     * <p>You may customize this method to release references to session data
     * or resources that can not be serialized with the session itself.</p>
     */
    @Override
    public void passivate() {
    }

    /**
     * <p>This method is called when the session containing it was
     * reactivated.</p>
     * 
     * <p>You may customize this method to reacquire references to session
     * data or resources that could not be serialized with the
     * session itself.</p>
     */
    @Override
    public void activate() {
    }

    /**
     * <p>This method is called when this bean is removed from
     * session scope.  Typically, this occurs as a result of
     * the session timing out or being terminated by the application.</p>
     * 
     * <p>You may customize this method to clean up resources allocated
     * during the execution of the <code>init()</code> method, or
     * at any later time during the lifetime of the application.</p>
     */
    @Override
    public void destroy() {
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     *
     * @return reference to the scoped data bean
     */
    protected ApplicationBean1 getApplicationBean1() {
        return (ApplicationBean1) getBean("ApplicationBean1");
    }
}
