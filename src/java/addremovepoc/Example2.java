/*
 * Example2.java
 *
 * Blue Lotus Software, LLC
 * 
 * $Id$
 * 
 * Copyright 2008 John Yeary 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *
 * Created on Sep 23, 2008, 5:33:49 PM
 */
package addremovepoc;

import com.sun.data.provider.impl.CachedRowSetDataProvider;
import com.sun.rave.web.ui.appbase.AbstractPageBean;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.FacesException;
import javax.faces.convert.IntegerConverter;
import javax.faces.event.ValueChangeEvent;

/**
 * <p>Page bean that corresponds to a similarly named JSP page.  This
 * class contains component definitions (and initialization code) for
 * all components that you have defined on this page, as well as
 * lifecycle methods and event handlers where you may add behavior
 * to respond to incoming events.</p>
 *
 * @author John Yeary
 * @version 1.0
 */
public class Example2 extends AbstractPageBean {
    // <editor-fold defaultstate="collapsed" desc="Managed Component Definition">

    /**
     * <p>Automatically managed component initialization.  <strong>WARNING:</strong>
     * This method is automatically generated, so any user-specified code inserted
     * here is subject to being replaced.</p>
     */
    private void _init() throws Exception {
        customerDataProvider1.setCachedRowSet((javax.sql.rowset.CachedRowSet) getValue("#{SessionBean1.customerRowSet1}"));
        customerDataProvider2.setCachedRowSet((javax.sql.rowset.CachedRowSet) getValue("#{SessionBean1.customerRowSet2}"));
    }
    private CachedRowSetDataProvider customerDataProvider1 = new CachedRowSetDataProvider();

    public CachedRowSetDataProvider getCustomerDataProvider1() {
        return customerDataProvider1;
    }

    public void setCustomerDataProvider1(final CachedRowSetDataProvider crsdp) {
        this.customerDataProvider1 = crsdp;
    }
    private CachedRowSetDataProvider customerDataProvider2 = new CachedRowSetDataProvider();

    public CachedRowSetDataProvider getCustomerDataProvider2() {
        return customerDataProvider2;
    }

    public void setCustomerDataProvider2(final CachedRowSetDataProvider crsdp) {
        this.customerDataProvider2 = crsdp;
    }
    private IntegerConverter addRemoveList1Converter = new IntegerConverter();

    public IntegerConverter getAddRemoveList1Converter() {
        return addRemoveList1Converter;
    }

    public void setAddRemoveList1Converter(final IntegerConverter ic) {
        this.addRemoveList1Converter = ic;
    }

    // </editor-fold>
    /**
     * <p>Construct a new Page bean instance.</p>
     */
    public Example2() {
    }

    /**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Customize this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     * 
     * <p>Note that, if the current request is a postback, the property
     * values of the components do <strong>not</strong> represent any
     * values submitted with this request.  Instead, they represent the
     * property values that were saved for this view when it was rendered.</p>
     */
    @Override
    public void init() {
        // Perform initializations inherited from our superclass
        super.init();
        // Perform application initialization that must complete
        // *before* managed components are initialized
        // TODO - add your own initialiation code here

        // <editor-fold defaultstate="collapsed" desc="Managed Component Initialization">
        // Initialize automatically managed components
        // *Note* - this logic should NOT be modified
        try {
            _init();
        } catch (final Exception e) {
            log("Page2 Initialization Failure", e);
            throw e instanceof FacesException ? (FacesException) e : new FacesException(e);
        }

    // </editor-fold>
    // Perform application initialization that must complete
    // *after* managed components are initialized
    // TODO - add your own initialization code here
    }

    /**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a postback request that
     * is processing a form submit.  Customize this method to allocate
     * resources that will be required in your event handlers.</p>
     */
    @Override
    public void preprocess() {
    }

    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a postback and then navigated to a different page).  Customize
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     */
    @Override
    public void prerender() {

        if (state == null) {
            customerDataProvider1.cursorFirst();
            state = (String) customerDataProvider1.getValue("STATE");
            try {
                getSessionBean1().getCustomerRowSet2().setString(1, state);
            } catch (final SQLException ex) {
                Logger.getLogger(Example2.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                getSessionBean1().getCustomerRowSet2().setString(1, state);
            } catch (final SQLException ex) {
                Logger.getLogger(Example2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        customerDataProvider2.refresh();
    }

    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called (regardless of whether
     * or not this was the page that was actually rendered).  Customize this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     */
    @Override
    public void destroy() {
        customerDataProvider1.close();
        customerDataProvider2.close();
    }

    protected SessionBean1 getSessionBean1() {
        return (SessionBean1) getBean("SessionBean1");
    }
    private String state;

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }
    private Integer[] selectedCustomers;

    public Integer[] getSelectedCustomers() {
        return selectedCustomers;
    }

    public void setSelectedCustomers(final Integer[] selectedCustomers) {
        this.selectedCustomers = selectedCustomers;
    }

    public void dropDown1_processValueChange(final ValueChangeEvent event) {
        state = (String) event.getNewValue();
        try {
            getSessionBean1().getCustomerRowSet2().setString(1, state);
        } catch (final SQLException ex) {
            Logger.getLogger(Example2.class.getName()).log(Level.SEVERE, null, ex);
        }
        customerDataProvider2.refresh();
    }

    public String button1_action() {
        for (Integer i : selectedCustomers) {
            info("Customer Number: " + i.toString());
        }
        return null;
    }
}

