<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Welcome
    Created on : Sep 28, 2008, 12:06:11 PM
    Author     : John Yeary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1" title="Welcome Page">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:hyperlink id="hyperlink1" style="left: 48px; top: 72px; position: absolute" text="Simple Add-Remove Component Example" url="/faces/Example1.jsp"/>
                        <webuijsf:hyperlink id="hyperlink2" style="left: 48px; top: 96px; position: absolute" text="Master-Detail Add-Remove Component Example" url="/faces/Example2.jsp"/>
                        <webuijsf:contentPageTitle id="contentArea1" style="left: 24px; top: 0px; position: absolute" title="Add-Remove Component Examples"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
