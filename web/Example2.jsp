<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Document   : Page2
Created on : Sep 23, 2008, 5:33:49 PM
Author     : John Yeary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1" title="Master-Detail Add-Remove Component Example">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:dropDown id="dropDown1" items="#{Example2.customerDataProvider1.options['CUSTOMER.STATE,CUSTOMER.STATE']}" label="State:"
                            onChange="webui.suntheme4_2.common.timeoutSubmitForm(this.form, 'dropDown1');" selected="#{Example2.state}"
                            style="left: 24px; top: 72px; position: absolute" valueChangeListenerExpression="#{Example2.dropDown1_processValueChange}"/>
                        <webuijsf:messageGroup id="messageGroup1" style="height: 46px; left: 576px; top: 48px; position: absolute; width: 238px"/>
                        <h:panelGrid id="gridPanel1" style="left: 24px; top: 120px; position: absolute">
                            <webuijsf:addRemove availableItemsLabel="Available:" converter="#{Example2.addRemoveList1Converter}" id="addRemoveList1"
                                items="#{Example2.customerDataProvider2.options['CUSTOMER.CUSTOMER_ID,CUSTOMER.NAME']}" label="Customers:" labelOnTop="true"
                                selectAll="true" selected="#{Example2.selectedCustomers}" selectedItemsLabel="Selected:"/>
                            <webuijsf:button actionExpression="#{Example2.button1_action}" id="button1" text="Submit"/>
                        </h:panelGrid>
                        <webuijsf:hyperlink id="hyperlink1" style="left: 24px; top: 24px; position: absolute" text="Home" url="/faces/Welcome.jsp"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
