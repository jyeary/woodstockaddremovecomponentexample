<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Document   : Page1
Created on : Sep 22, 2008, 2:05:00 PM
Author     : John Yeary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1" title="Add-Remove Component Example">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:button actionExpression="#{Example1.button1_action}" id="button1" style="left: 23px; top: 48px; position: absolute" text="Submit"/>
                        <webuijsf:messageGroup id="messageGroup1" style="left: 648px; top: 48px; position: absolute"/>
                        <webuijsf:addRemove availableItemsLabel="Available:" converter="#{Example1.addRemoveList1Converter}" id="addRemoveList1"
                                            items="#{Example1.customerDataProvider.options['CUSTOMER.CUSTOMER_ID,CUSTOMER.NAME']}" label="Customers:" labelOnTop="true"
                                            moveButtons="true" selectAll="true" selected="#{Example1.selected}" selectedItemsLabel="Selected:" sorted="true" style="left: 24px; top: 96px; position: absolute"/>
                        <webuijsf:hyperlink id="hyperlink1" style="left: 24px; top: 24px; position: absolute" text="Home" url="/faces/Welcome.jsp"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
